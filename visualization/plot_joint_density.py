import numpy as np
import seaborn as sns
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
def hide_current_axis(*args, **kwds):
    plt.gca().set_visible(False)

def plot_joint_density(data, rowvar = False, labels = None, reference = None, scatter = True, alpha = 0.1):
    if rowvar:
        data = np.transpose(data)
    if labels is None:
        labels = [r"$x_%d$"%(i) for i in range (data.shape[1])]
    df = pd.DataFrame(data, columns=labels)
    g = sns.PairGrid(df)
    g = g.map_lower(sns.kdeplot, cmap=None, colors = "k", linewidths = 1, levels = 5)
    if scatter:
        g = g.map_lower(sns.scatterplot, s = 5, linewidth = 0, alpha = alpha,  color = 'C0')
    else:
        g = g.map_lower(sns.histplot,  color = 'C0', stat = "density")
        # g = g.map_lower(sns.histplot,  color = 'C0', bins = 25, stat = "density")
    g = g.map_diag(sns.histplot, edgecolor = "black", color = "k" , element = "step",\
                   fill = True, alpha = 0.0, )
    g = g.map_upper(hide_current_axis)

    idx = 0
    ax = g.axes.flat
    size = int(np.sqrt(len(ax)))
    for row in range(size):
        for col in range(size):
            if col <= row:
                if not (reference is None):
                    ax[idx].axvline(x = reference[col], linestyle = "--", color = "r")
                ax[idx].spines['top'].set_visible(True)
                ax[idx].spines['right'].set_visible(True)
            if col < row:
                if not (reference is None):
                    ax[idx].axhline(y = reference[row], linestyle = "--", color = "r")
            ax[idx].tick_params(axis='x', labelrotation = 30)
            ax[idx].tick_params(axis='y', labelrotation = 30)
            idx += 1
    ax[0].set_yticks([])
    ax[0].set_ylabel("")
