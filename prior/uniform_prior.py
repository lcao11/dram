# This file is a complement of the prior model available in hippylib.
# This file includes a uniform prior that compatible with the hippylib _Prior class
# Both works only for finite dimensional parameters.
import sys, os
sys.path.append(os.environ.get('HIPPYLIB_PATH'))
from hippylib import Operator2Solver
import dolfin as dl
import math
import numpy as np
import ufl
import scipy.linalg as scila

class UniformPrior:
    """
    This class implements a finite-dimensional uniform prior.
    """

    def __init__(self, Vh, upper_bound, lower_bound = None):
        """
        Constructor
        Inputs:
        - :code:`Vh`:             Finite element space on which the prior is
                                  defined. Must be the Real vector space with finite global
                                  degree of freedom
        - :code:`upper_bound`:    The upper bound of the uniform prior. Must be a
                                  :code:`numpy.array` of appropriate size.
        - :code:`lower bound:     The lower bound of the uniform prior. Must be a
                                    :code:`numpy.array` of appropriate size.
        """

        self.Vh = Vh
        self._dim = Vh.dim()

        # Check if upper bound is a float array compatible with the given parameter space
        if not np.issubdtype(upper_bound.dtype, np.floating):
            raise TypeError("Upper bound be a float array")
        elif  self._dim != upper_bound.size:
            raise ValueError("Upper bound incompatible with Finite Element space")

        # Check if lower bound is a float array compatible with the given parameter space.
        # If not given, then use zero as lower bound.
        if lower_bound is None:
            lower_bound = np.zeros(self._dim)
        elif not np.issubdtype(lower_bound.dtype, np.floating):
            raise TypeError("Lower bound must be a float array")
        elif self._dim != lower_bound.size:
            raise ValueError("Lower bound incompatible with Finite Element space")

        # Check if upper bound is above lower bound
        if np.less(upper_bound, lower_bound).any():
            raise ValueError("The upper bound values must be bigger than the lower bound values.")

        self._covariance = np.diag(upper_bound-lower_bound)**2

        # Negative log-pdf evaluation of the uniform distribution
        self._cost = math.log(np.prod(upper_bound-lower_bound))

        self._chol = np.linalg.cholesky(self._covariance)

        self._chol_inv = scila.solve_triangular(
                                        self._chol,
                                        np.identity(self._dim),
                                        lower=True)

        self._precision = np.dot(self._chol_inv.T, self._chol_inv)

        trial = dl.TrialFunction(Vh)
        test  = dl.TestFunction(Vh)
        
        domain_measure = dl.assemble(dl.Constant(1.) * ufl.dx(Vh.mesh()))
        domain_measure_inv = dl.Constant(1.0/domain_measure)

        #Identity mass matrix
        self._M = dl.assemble(domain_measure_inv * ufl.inner(trial, test) * ufl.dx)
        self._Msolver = Operator2Solver(self._M)

        if self._dim == 1:
            trial = ufl.as_matrix([[trial]])
            test  = ufl.as_matrix([[test]])

        #Create form matrices
        covariance_op = ufl.as_matrix(list(map(list, self._covariance)))
        precision_op  = ufl.as_matrix(list(map(list, self._precision)))
        chol_op       = ufl.as_matrix(list(map(list, self._chol)))
        chol_inv_op   = ufl.as_matrix(list(map(list, self._chol_inv)))

        #variational for the regularization operator, or the precision matrix
        var_form_R = domain_measure_inv \
                     * ufl.inner(test, ufl.dot(precision_op, trial)) * ufl.dx

        #variational form for the square root of the regularization operator
        var_form_R_sqrt = domain_measure_inv \
                          * ufl.inner(test, ufl.dot(chol_inv_op.T, trial)) * ufl.dx

        #variational form for the square root of the inverse regularization
        #operator
        var_form_Rinv_sqrt = domain_measure_inv \
                             * ufl.inner(test, ufl.dot(chol_op, trial)) * ufl.dx

        self.R         = dl.assemble(var_form_R)
        self.sqrtR     = dl.assemble(var_form_R_sqrt)
        self.sqrtRinv  = dl.assemble(var_form_Rinv_sqrt)
        
        self._lower_bound = dl.Vector(self.R.mpi_comm())
        self.init_vector(self._lower_bound, 1)
        self._lower_bound.set_local(lower_bound)

        # the dummy parameter for normalization
        self._m_nm = dl.Vector(self.R.mpi_comm())
        self.init_vector(self._m_nm, 0)

        # the dummy parameter for subtraction
        self._m_sub = dl.Vector(self.R.mpi_comm())
        self.init_vector(self._m_sub, 0)
        
    def init_vector(self, x, dim):
        """
        Inizialize a vector :code:`x` to be compatible with the
        range/domain of :math:`R`.
        If :code:`dim == "noise"` inizialize :code:`x` to be compatible
        with the size of white noise used for sampling.
        """

        if dim == "noise":
            self.sqrtRinv.init_vector(x, 1)
        else:
            self.sqrtRinv.init_vector(x, dim)

    def sample(self, noise, s, add_mean = True):
        """
        Given :code:`noise` :math:`\\sim \\mathcal{U}(0, 1)^{dim}` compute a
        sample :code:`s` from the prior.
        """
       
        self.sqrtRinv.mult(noise, s)
        if add_mean:
            s.axpy(1.0, self._lower_bound)
    
    def check_bounds(self, m):
        self._m_nm.zero()
        self._m_sub.zero()
        self._m_sub.axpy(1., m)
        self._m_sub.axpy(-1., self._lower_bound)
        self.sqrtR.mult(self._m_sub, self._m_nm)
        for i in range(self._dim):
            if self._m_nm[i] < 0.0 or self._m_nm[i] > 1.0:
                return False
        return True
            
    def cost(self, m):
        """
        Given :code:`m` : a vector of parameters of type `dolfin.Vector()` compute the negative logpdf.
        """
        
        if self.check_bounds(m):
            return self._cost
        else:
            return math.inf
        
        
        
