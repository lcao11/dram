import numpy as np
import sys, os
sys.path.append(os.environ.get('HIPPYLIB_PATH'))
import dolfin as dl
import math
import scipy.stats as ss
from hippylib import GaussianRealPrior
from .uniform_prior import UniformPrior

class KDEPrior(GaussianRealPrior):
    def __init__(self, Vh, prior_data, upper_bound = None, lower_bound = None):
        
        self.param_dim = Vh.dim()
        self.data = prior_data
        if self.param_dim == self.data.ndim and self.param_dim == 1:
            pass
        elif self.param_dim == prior_data.shape[0] :
            pass
        elif self.param_dim == self.data.shape[1]:
            self.data = np.transpose(self.data)
        else:
            raise ValueError("The Dimension of the parameter space does not match the data.")

        if self.param_dim == 1:
            self.n_samples = self.data.size
        else:
            _, self.n_samples = self.data.shape

        self.kde = ss.gaussian_kde(self.data)

        super(KDEPrior, self).__init__(Vh, self.kde.covariance)
        if self.param_dim == 1:
            data_mean = np.array([np.mean(self.data)])
        else:
            data_mean = np.mean(self.data, axis = 1)
        self.mean = dl.Vector()
        self.init_vector(self.mean, 1)
        self.mean.set_local(data_mean)
        
        self.help = dl.Vector()
        self.init_vector(self.help, 1)

        self._bounded = False
        if not upper_bound is None:
            self._uniform_prior = UniformPrior(Vh, upper_bound, lower_bound = lower_bound)
            self._bounded = True
            self._scale = self.kde.integrate_box(lower_bound, upper_bound)

    def sample(self, noise, s, add_mean=True):
        """
        Given :code:`noise` :math:`\\sim \\mathcal{N}(0, I)` compute a
        sample :code:`s` from the prior.
        If :code:`add_mean == True` add the prior mean value to :code:`s`.
        """
        outside = True
        while outside:
            self.sqrtRinv.mult(noise, s)
            if self.param_dim == 1:
                sample_mean = np.array([self.data[np.random.choice(self.n_samples)]])
            else:
                sample_mean = self.data[:, np.random.choice(self.n_samples)]
            self.help.set_local(sample_mean)
            s.axpy(1.0, self.help)
            if not self._bounded:
                outside = False
            elif self._uniform_prior.check_bounds(s):
                outside = False
            if not add_mean:
                s.axpy(-1.0, self.mean)
    
    def cost(self, m):

        if self._bounded:
            if self._uniform_prior.check_bounds(m):
                return -self.kde.logpdf(m.get_local()) + math.log(self._scale)
            else:
                return math.inf
        else:
            return -self.kde.logpdf(m.get_local())
        
    
    
    
    
