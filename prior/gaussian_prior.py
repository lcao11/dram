import numpy as np
import sys, os
sys.path.append(os.environ.get('HIPPYLIB_PATH'))
import dolfin as dl
import math
import scipy.stats as ss
from hippylib import GaussianRealPrior
from .uniform_prior import UniformPrior


class GaussianPrior:
    def __init__(self, Vh, cov, mean, upper_bound = None, lower_bound = None):

        self.param_dim = Vh.dim()
        self.gaussian = GaussianRealPrior(Vh, cov, mean)
        self._bounded = False

        if (upper_bound is None and not upper_bound is None) or (not upper_bound is None and upper_bound is None):
            raise ValueError("Please set both upper and lower bounds for cost evaluation.")

        if not upper_bound is None and not upper_bound is None:
            self._uniform_prior = UniformPrior(Vh, upper_bound, lower_bound=lower_bound)
            self._bounded = True

    def init_vector(self, x, dim):
        """
        Inizialize a vector :code:`x` to be compatible with the
        range/domain of :math:`R`.
        If :code:`dim == "noise"` inizialize :code:`x` to be compatible
        with the size of white noise used for sampling.
        """
        self.gaussian.init_vector(x, dim)

    def sample(self, noise, s, add_mean=True):
        """
        Given :code:`noise` :math:`\\sim \\mathcal{N}(0, I)` compute a
        sample :code:`s` from the prior.
        If :code:`add_mean == True` add the prior mean value to :code:`s`.
        """
        self.gaussian.sample(noise, s, add_mean = add_mean)

    def cost(self, m):
        if (self._bounded and self._uniform_prior.check_bounds(m)) or not self._bounded:
            return self.gaussian.cost(m)
        else:
            return math.inf






