import numpy as np
import sys, os
sys.path.append(os.environ.get('HIPPYLIB_PATH'))
import dolfin as dl
import ufl
import math
import scipy.stats as ss
from scipy.special import erfc
from .kde_prior import KDEPrior
from .uniform_prior import UniformPrior
from .gaussian_prior import GaussianPrior

class MixedPrior:
    def __init__(self, Vh):
        self.Vh = Vh
        self._param_dim = Vh.dim()
        self._kde_prior = None
        self._uniform_prior = None
        self._gaussian_prior = None
        self._help_uniform = [None] * 2
        self._help_gaussian = [None] * 2
        self._help_kde = [None]*2

        trial = dl.TrialFunction(Vh)
        test = dl.TestFunction(Vh)
        self.R = dl.assemble(ufl.inner(trial, test) * ufl.dx)

    def init_vector(self, x, dim):
        """
        Inizialize a vector :code:`x` to be compatible with the
        range/domain of :math:`R`.
        If :code:`dim == "noise"` inizialize :code:`x` to be compatible
        with the size of white noise used for sampling.
        """

        if dim == "noise":
            self.R.init_vector(x, 1)
        else:
            self.R.init_vector(x, dim)

    def check_index(self, index):
        if not all([isinstance(index[i], int) for i in range(len(index))]):
            raise ValueError("The list of prior input indices are not integers.")
        elif min(index) < 0:
            raise ValueError("Prior input indices cannot be negative.")
        elif max(index) >= self._param_dim:
            raise ValueError("Prior input indices cannot be larger than or equal to the parameter dimension.")
        elif len(index) != len(set(index)):
            raise ValueError("Duplicated indices.")

    def check_definition(self):
        index = []
        if not self._uniform_prior is None:
            index.extend(self._index_uniform)
        if not self._gaussian_prior is None:
            index.extend(self._index_gaussian)
        if not self._kde_prior is None:
            index.extend(self._index_kde)
        if len(index) != len(set(index)):
            raise ValueError("Duplicated indices for different prior components.")
        if len(index)!=self._param_dim:
            raise ValueError("Missing prior components.")

    def set_uniform_prior(self, index, upper_bound, lower_bound = None):
        self.check_index(index)
        self._index_uniform = index
        self._Vh_uniform = dl.VectorFunctionSpace(self.Vh.mesh(), "R", degree=0, dim=len(index))
        if lower_bound is None:
            self._uniform_prior = UniformPrior(self._Vh_uniform, upper_bound)
        else:
            self._uniform_prior = UniformPrior(self._Vh_uniform, upper_bound, lower_bound)
        self._help_uniform = [dl.Vector() for i in range(2)]
        for i in range(2):
            self._uniform_prior.init_vector(self._help_uniform[i], 1)

    def set_gaussian_prior(self, index, cov, mean, upper_bound = None, lower_bound = None):
        self.check_index(index)
        self._index_gaussian = index
        self._Vh_gaussian = dl.VectorFunctionSpace(self.Vh.mesh(), "R", degree=0, dim=len(index))
        self._mean = dl.Function(self._Vh_gaussian).vector()
        self._mean.set_local(mean)
        self._gaussian_prior = GaussianPrior(self._Vh_gaussian, cov, self._mean, upper_bound=upper_bound, lower_bound = lower_bound)
        self._help_gaussian = [dl.Vector() for i in range(2)]
        for i in range(2):
            self._gaussian_prior.init_vector(self._help_gaussian[i], 1)

    def set_kde_prior(self, index, data, upper_bound = None, lower_bound = None):
        self.check_index(index)
        self._index_kde = index
        self._Vh_kde = dl.VectorFunctionSpace(self.Vh.mesh(), "R", degree=0, dim=len(index))
        self._kde_prior = KDEPrior(self._Vh_kde, data, upper_bound = upper_bound, lower_bound = lower_bound)
        self._help_kde = [dl.Vector() for i in range(2)]
        for i in range(2):
            self._kde_prior.init_vector(self._help_kde[i], 1)

    def gaussian2uniform(self, noise):

        temp = 0.5*erfc(-noise.get_local()/math.sqrt(2))
        noise.set_local(temp)

    def divide_parameters(self, out_u, out_g, out_k, m):
        temp = m.get_local()
        if not self._uniform_prior is None:
            out_u.set_local(temp[self._index_uniform])
        if not self._gaussian_prior is None:
            out_g.set_local(temp[self._index_gaussian])
        if not self._kde_prior is None:
            out_k.set_local(temp[self._index_kde])

    def gather_parameters(self, out, m_u, m_g, m_k):
        temp = out.get_local()
        if not self._uniform_prior is None:
            temp[self._index_uniform] = m_u.get_local()
        if not self._gaussian_prior is None:
            temp[self._index_gaussian] = m_g.get_local()
        if not self._kde_prior is None:
            temp[self._index_kde] = m_k.get_local()
        out.set_local(temp)

    def sample(self, noise, s, add_mean=True):
        """
        Given :code:`noise` :math:`\\sim \\mathcal{N}(0, I)` compute a
        sample :code:`s` from the prior.
        If :code:`add_mean == True` add the prior mean value to :code:`s`.
        """
        self.check_definition()
        self.divide_parameters(self._help_uniform[0], self._help_gaussian[0], self._help_kde[0], noise)
        if not self._uniform_prior is None:
            self.gaussian2uniform(self._help_uniform[0])
            self._uniform_prior.sample(self._help_uniform[0], self._help_uniform[1], add_mean = add_mean)
        if not self._gaussian_prior is None:
            self._gaussian_prior.sample(self._help_gaussian[0], self._help_gaussian[1], add_mean = add_mean)
        if not self._kde_prior is None:
            self._kde_prior.sample(self._help_kde[0], self._help_kde[1], add_mean = add_mean)
        self.gather_parameters(s, self._help_uniform[1], self._help_gaussian[1], self._help_kde[1])

    def cost(self, m):

        self.check_definition()
        self.divide_parameters(self._help_uniform[0], self._help_gaussian[0], self._help_kde[0], m)
        temp = 0.0
        if not self._uniform_prior is None:
            temp += self._uniform_prior.cost(self._help_uniform[0])
        if not self._gaussian_prior is None:
            temp += self._gaussian_prior.cost(self._help_gaussian[0])
        if not self._kde_prior is None:
            temp += self._kde_prior.cost(self._help_kde[0])
        return temp





