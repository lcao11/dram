import numpy as np
import os

STATE = 0
PARAMETER = 1

class Tracer:
    def __init__(self, Vh, n_samples, out_number, out_dir):
        self._param_dim = Vh[PARAMETER].dim()
        if self._param_dim == 1:
            self.samples = np.zeros(1)
            self.covariance = np.zeros(1)
        else:
            self.samples = np.zeros((1, self._param_dim))
            self.covariance = np.zeros((1, self._param_dim, self._param_dim))
        self.qoi = np.zeros(1)
        self.ar = np.zeros(1)
        self.cost = np.zeros(1)

        self._idx_accepted = 0
        self._idx = 0
        self._out_freq = n_samples/out_number

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        
        self._out_dir = out_dir
        
    def append(self, current, q_value, covariance, result):
        self._idx += 1
        self._idx_accepted += result
        if self._idx == 1:
            self.ar[0] = self._idx_accepted / float(self._idx) * 100
            if self._param_dim == 1:
                self.samples[0] = current.m.get_local()
                self.covariance[0] = covariance
            else:
                self.samples[0, :] = current.m.get_local()
                self.covariance[0, :, :] = covariance
            self.cost[0] = current.cost
            self.qoi[0] = q_value
        else:
            self.ar = np.append(self.ar, self._idx_accepted / float(self._idx) * 100)
            if self._param_dim == 1:
                self.samples = np.append(self.samples, current.m.get_local())
            else:
                self.samples = np.append(self.samples, np.expand_dims(current.m.get_local(), axis=0), axis=0)
            self.cost = np.append(self.cost, current.cost)
            self.qoi = np.append(self.qoi, q_value)
            self.covariance = np.append(self.covariance, np.expand_dims(covariance, axis=0), axis=0)
        if self._idx % self._out_freq == 0:
            self.save()

    def save(self):
        np.save(self._out_dir + '/samples.npy', self.samples)
        np.save(self._out_dir + '/acceptance_rate.npy', self.ar)
        np.save(self._out_dir + '/cost.npy', self.cost)
        np.save(self._out_dir + '/qoi.npy', self.qoi)
        np.save(self._out_dir + '/covariance.npy', self.covariance)
        
