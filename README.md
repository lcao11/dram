**Delayed-Rejection Adaptive Metropolis**

The implementation of Delayed-Rejection Adaptive Metropolis algorithm for Bayesian inference problem. The code is based on the hIPPYlib library and the FEniCS library.
