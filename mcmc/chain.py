STATE = 0
PARAMETER = 1

class SampleStruct:
    def __init__(self, kernel):
        self.u = kernel.model.generate_vector(STATE)
        self.m = kernel.model.generate_vector(PARAMETER)
        self.cost = 0
        
    def assign(self, other):
        self.cost = other.cost
        self.m = other.m.copy()
        self.u = other.u.copy()

class DRAM_MCMC:
    def __init__(self, kernel):
        self.kernel = kernel
        self.parameters = {}
        self.parameters["number_of_samples"] = 5000
        self.parameters["print_progress"] = 20
        self.parameters["print_level"] = 1
        
    def run(self, m0, tracer, qoi = None):

        self.kernel.init_kernel()

        number_of_samples = self.parameters["number_of_samples"]
        
        current = SampleStruct(self.kernel)
        proposed = [SampleStruct(self.kernel) for i in range\
        (self.kernel.parameters["maximum_proposals"])]
        
        current.m.zero()
        current.m.axpy(1., m0)
        self.kernel.init_sample(current)

        if not qoi is None:
            q_value = qoi.eval(current)
        else:
            q_value = 0
        tracer.append(current, q_value, self.kernel.proposal_kernel.cov, 0)
        self.kernel.update(current)
            
        if self.parameters["print_level"] > 0:
            print( "Generate {0} samples".format(number_of_samples))
        sample_count = 0
        naccept = 0
        n_check = number_of_samples // self.parameters["print_progress"]
        q_value = 0
        while (sample_count < number_of_samples):
            update = self.kernel.sample(current, proposed)
            naccept += update
            self.kernel.update(current)
            if not qoi is None:
                q_value = qoi.eval(current)
            else:
                q_value = 0
            tracer.append(current, q_value, self.kernel.proposal_kernel.cov, update)
            sample_count += 1
            if sample_count % n_check == 0 and self.parameters["print_level"] > 0:
                print("{0:2.2f} % completed, Acceptance ratio {1:2.2f} %".format(float(sample_count)/float(number_of_samples)*100,
                                                                         float(naccept)/float(sample_count)*100))
        return naccept, self.kernel.n_solves
    
    def consume_random(self):
        for i in range(self.parameters["number_of_samples"]):
            self.kernel.consume_random()
