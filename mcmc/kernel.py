import math
import numpy as np
import dolfin as dl


def logexpm1(a):
    if a < 35:
        return math.log(np.expm1(a))
    else:
        return a


class GaussianProposal:
    def __init__(self, dim):
        self._dim = dim
        self.cov = np.zeros((self._dim, self._dim))

    def set_covariance(self, cov):
        self.cov = cov

    def sample(self, s):
        s.set_local(np.random.multivariate_normal(np.zeros(self._dim), self.cov))


class DRAMKernel:
    def __init__(self, model, proposal_covariance):
        self._dim = model.prior.Vh.dim()
        self.model = model
        self.init_cov = proposal_covariance
        self.parameters = {}
        self.parameters["non-adaptive_samples"] = 20
        self.parameters["maximum_proposals"] = 3
        self.parameters["DR_proposal_scale"] = 0.2
        self.parameters["next_stage_probability"] = 0.5
        if not proposal_covariance.shape[0] == self._dim or\
                not proposal_covariance.shape[1] == self._dim:
            raise IndexError("The shape of the proposal covariance is, ", proposal_covariance.shape,
                             "not matching the dimension of the parameter space, ", self._dim())

        self.proposal_kernel = GaussianProposal(self._dim)

        self._sd = 2.38 ** 2 /self._dim
        self.n_solves = 0

    def name(self):
        return "Delayed Rejection Adptive Metropolis"

    def derivativeInfo(self):
        return 0

    def init_kernel(self):
        self._am_count = 0
        self._am_mean = np.zeros(self._dim)
        self._am_cov = np.zeros((self._dim, self._dim))
        self.proposal_kernel.set_covariance(self.init_cov)

    def init_sample(self, current):
        if math.isinf(self.model.prior.cost(current.m)):
            current.cost = math.inf
        else:
            self.model.solveFwd(current.u, [current.u, current.m, None])
            current.cost = self.model.cost([current.u, current.m, None])[0]

    def sample(self, current, proposed):
        max_stages = self.parameters["maximum_proposals"]
        scale = np.ones(max_stages)
        scale *= self.parameters["DR_proposal_scale"]
        scale[0] = 1.0
        min_cost = math.inf
        for i in range(max_stages):
            if i == 0:
                self.proposal(proposed[i], current, scale=scale[i])
            else:
                if proposed[i-1].cost < min_cost:
                    min_cost = proposed[i-1].cost
                self.proposal(proposed[i], proposed[i - 1], scale=scale[i])
            self.init_sample(proposed[i])
            self.n_solves += 1
            if math.isinf(proposed[i].cost):
                accepted = 0
            elif current.cost - proposed[i].cost >= 0:
                current.assign(proposed[i])
                accepted = 1
                break
            elif proposed[i].cost - min_cost > 0:
                accepted = 0
            elif min_cost - proposed[i].cost  >= min_cost - current.cost and not math.isinf(min_cost):
                current.assign(proposed[i])
                accepted = 1
                break
            else:
                if math.isinf(min_cost):
                    al = current.cost - proposed[i].cost
                else:
                    al = logexpm1(min_cost - proposed[i].cost) \
                         - logexpm1(min_cost - current.cost)
                if (al > math.log(np.random.rand())):
                    current.assign(proposed[i])
                    accepted = 1
                    break
                else:
                    accepted = 0

            if np.random.rand() >= self.parameters["next_stage_probability"]:
                break

        return accepted

    def proposal(self, proposed, current, scale=1.0):

        proposed.m.zero()
        self.proposal_kernel.sample(proposed.m)
        proposed.m *= scale
        proposed.m.axpy(1., current.m)

    def update(self, current):

        m_k = current.m.get_local()
        self._am_mean *= self._am_count / (self._am_count + 1)
        self._am_mean += m_k / (self._am_count + 1)

        if self._am_count <= self.parameters["non-adaptive_samples"]:
            self._am_cov +=  np.outer(m_k, m_k)

        if self._am_count == self.parameters["non-adaptive_samples"]:
            self._outer_mean = np.outer(self._am_mean, self._am_mean)
            self._am_cov -= self._outer_mean*(self._am_count+1)
            self._am_cov *= self._sd / self._am_count

        if self._am_count > self.parameters["non-adaptive_samples"]:
            self._am_cov *= (self._am_count - 1) / self._am_count
            self._am_cov += self._sd * self._outer_mean
            self._am_cov += self._sd * np.outer(m_k, m_k) /self._am_count
            self._outer_mean = np.outer(self._am_mean, self._am_mean)
            self._am_cov -= self._sd *(self._am_count + 1)* self._outer_mean/self._am_count
            self.proposal_kernel.set_covariance(self._am_cov)

        self._am_count += 1

    def consume_random(self):
        for i in range(self.parameters["maximum_proposals"] * 2):
            np.random.rand()
        np.random.multivariate_normal(np.zeros(self._dim), np.diag(np.ones(self._dim)))
