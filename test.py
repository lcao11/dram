import dolfin as dl
import hippylib as hl
import numpy as np
from uniform_prior import UniformPrior
import matplotlib.pyplot as plt
from dram_kernel import DRAMKernel
from mcmc_tracer import FullTracer
from dram_mcmc import DRAM_MCMC

STATE = 0
PARAMETER = 1

class random_generator:
    def __init__(self, Vh):
        self._s = Vh.dim()
    def normal(self, sigma, mean, out):
        out.zero()
        out.set_local(np.random.normal(scale = sigma, loc = mean, size = self._s))
    def uniform(self, a, b, out):
        out.zero()
        out.set_local(np.random.uniform(a, b, self._s))
        
class misfit:
    def __init__(self, Vh, mean, sigma):
        self._mean = mean
        self._sigma = sigma
    def cost(self, x):
        g = np.zeros(2)
        g[0] = x[PARAMETER][0]
        g[1] = x[PARAMETER][0]**2 + x[PARAMETER][1] + 1
        return 0.5*np.sum(np.divide((g-self._mean)**2, self._sigma**2))

class forward:
    def __init__(self, Vh):
        self.Vh= Vh
    def generate_state(self):
        return dl.Function(self.Vh[STATE]).vector()
    def generate_parameter(self):
        return dl.Function(self.Vh[PARAMETER]).vector()
    def solveFwd(self, out, x):
        out.zero()
        out.axpy(1., x[PARAMETER])

mesh = dl.RectangleMesh(dl.Point(0,0), dl.Point(40,40), 100, 100)
param_dim = 2
Vh_STATE = dl.VectorFunctionSpace(mesh, "R", degree=0, dim=param_dim)
Vh_PARAMETER = dl.VectorFunctionSpace(mesh, "R", degree=0, dim=param_dim)
Vh = [Vh_STATE, Vh_PARAMETER]
rg = random_generator(Vh_PARAMETER)

upper_bound = np.array([5, 5]).astype(float)
lower_bound = np.array([-5, -5]).astype(float)
prior = UniformPrior(Vh_PARAMETER, upper_bound, lower_bound)
problem = forward(Vh)
llh = misfit(Vh_STATE, np.array([0., 0.]), np.array([1., 1.]))

noise, m0 = dl.Vector(), dl.Vector()
prior.init_vector(noise, "noise")
prior.init_vector(m0, 1)
rg.uniform(0, 1, noise)
prior.sample(noise, m0)
print(m0.get_local())

model = hl.Model(problem, prior, llh)
kernel_DRAM = DRAMKernel(model, np.array([1., 1.]).astype(float))
kernel_DRAM.parameters["maximum_proposals"]  = 2
kernel_DRAM.parameters["next_stage_probability"]  = 1.
chain = DRAM_MCMC(kernel_DRAM)
chain.parameters["number_of_samples"] = 50000
chain.parameters["print_progress"] = 10
tracer = FullTracer(Vh, 100000)
n_accept = chain.run(m0, tracer)
tracer.save()


accepted = np.load("accepted.npy")
rejected = np.load("rejected.npy")
cost = np.load("cost.npy")

plt.figure(0)
plt.plot(accepted[:, 0], '-*')
plt.plot(accepted[:, 1], '-^')
plt.title("Accepted samples")
plt.figure(1)
plt.plot(cost, '-*')
plt.title("Cost")
plt.figure(2)
plt.plot(rejected[:, 0], '-*')
plt.plot(rejected[:, 1], '-^')
plt.title("Rejected samples")

import seaborn as sns
import pandas as pd
from scipy import stats

plt.figure(3)
sns.distplot(accepted[accepted.shape[0]//2:, 0])
plt.figure(4)
sns.distplot(accepted[accepted.shape[0]//2:, 1])

plt.figure(5)
df = pd.DataFrame(accepted[accepted.shape[0]//2:, :], columns=["m1", "m2"])
sns.jointplot(x="m1", y="m2", data=df, kind="kde")


plt.show()
